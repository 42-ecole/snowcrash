#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define BUFF_SIZE 100

int		main(int argc, char *argv[])
{
	char b[BUFF_SIZE + 1];

	if (argc != 2)
		return (1);
	int	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
		return (1);
	else
	{
		int ret = read(fd,b, BUFF_SIZE);
		--ret;
		b[ret] = '\0';
		for (int i = 0; i < ret; i++)
		{
			b[i] -= i;
			printf("%c", b[i]);
		}
		b[i] = '\n';
	}
	return (0);
}
